var annotated_dup =
[
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "drv8847", null, [
      [ "DRV8847", "classdrv8847_1_1DRV8847.html", "classdrv8847_1_1DRV8847" ],
      [ "Motor", "classdrv8847_1_1Motor.html", "classdrv8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ]
];