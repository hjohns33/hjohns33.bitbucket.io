var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "drv8847.py", "drv8847_8py.html", "drv8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "homeworkpage.py", "homeworkpage_8py.html", null ],
    [ "lab0x00.py", "lab0x00_8py.html", "lab0x00_8py" ],
    [ "lab0x01.py", "lab0x01_8py.html", "lab0x01_8py" ],
    [ "lab0x02.py", "lab0x02_8py.html", "lab0x02_8py" ],
    [ "lab0x03.py", "lab0x03_8py.html", "lab0x03_8py" ],
    [ "lab0x04.py", "lab0x04_8py.html", "lab0x04_8py" ],
    [ "lab0xff.py", "lab0xff_8py.html", "lab0xff_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", "task__encoder_8py" ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ]
];