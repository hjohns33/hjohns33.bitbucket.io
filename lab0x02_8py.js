var lab0x02_8py =
[
    [ "AR", "lab0x02_8py.html#aa525b06d0cbe21f5c98e1ecd69a72d0a", null ],
    [ "currentTime", "lab0x02_8py.html#a63bb57e07884633cb15f94df92b58edb", null ],
    [ "dataQ1", "lab0x02_8py.html#acdebaade905757ce69746141d7522376", null ],
    [ "dataQ2", "lab0x02_8py.html#a7cefd9899bada5509486d63ecb5a8908", null ],
    [ "delta1", "lab0x02_8py.html#aafeecf0ad757b88483c28801e863195b", null ],
    [ "delta2", "lab0x02_8py.html#a33c35d144fc9b3d5dd3c238723c69b33", null ],
    [ "enc1", "lab0x02_8py.html#aebb472cdd0ffbcf6e50658527ec16a38", null ],
    [ "enc1_pos", "lab0x02_8py.html#a466288062439c56a1252170719073b19", null ],
    [ "enc1T", "lab0x02_8py.html#aac830744d343bc28cadc4a6d12ecc674", null ],
    [ "enc2", "lab0x02_8py.html#a44f4828a3c999f66e835c2f0f5d5a125", null ],
    [ "enc2_pos", "lab0x02_8py.html#ae7d9e309a99a0dafb6db72ac9ae35259", null ],
    [ "enc2T", "lab0x02_8py.html#ac7841011c1bea22bb79fc6cb18cda666", null ],
    [ "f", "lab0x02_8py.html#a3517c9427758fde5dd885ac4e255618e", null ],
    [ "interrupt1", "lab0x02_8py.html#a5a82e819d1ff6849e5afc712ecc1510a", null ],
    [ "interrupt2", "lab0x02_8py.html#a0bc3e94b7cd904ff3180df6d1d21e4fe", null ],
    [ "pin1A", "lab0x02_8py.html#a542596c122ed86541251aa25353ef84d", null ],
    [ "pin1B", "lab0x02_8py.html#acfd68ecbfbcd0c9f5ae4d431a467b888", null ],
    [ "pin2A", "lab0x02_8py.html#a8e2306adda458e7e8bd46d919f953cdf", null ],
    [ "pin2B", "lab0x02_8py.html#ac76c83af578bf8ca315555a96529be36", null ],
    [ "resetflag1", "lab0x02_8py.html#a47435d7f42c3f7536422f586873b86b5", null ],
    [ "resetflag2", "lab0x02_8py.html#afaa94e609dc766f45bb3fe43eed58249", null ],
    [ "runflag1", "lab0x02_8py.html#af0f0e2848375284ef309f4909e9f8c67", null ],
    [ "runflag2", "lab0x02_8py.html#a139829b9ba65f98b22f9da114a75b476", null ],
    [ "startTime1", "lab0x02_8py.html#a0c721b36ee0dd6ea5b3a0d63e8ea0c81", null ],
    [ "startTime2", "lab0x02_8py.html#ac3fbeebe46669aeeecce859755c4367f", null ],
    [ "timer1", "lab0x02_8py.html#a95a5bd490fdda5a0ea7212bea54a4b6d", null ],
    [ "timer1Ch1", "lab0x02_8py.html#a89d407f0574b578d4ac3126438659966", null ],
    [ "timer1Ch2", "lab0x02_8py.html#a31c2e10113dc07f3f7829161062d2c62", null ],
    [ "timer2", "lab0x02_8py.html#a37e6f7d97a4ccc7783b1ae162a2449cc", null ],
    [ "timer2Ch1", "lab0x02_8py.html#a92491d1fc13b1627d00f18141174b6e2", null ],
    [ "timer2Ch2", "lab0x02_8py.html#a62d2222f3f7129fe6a26d2d005285d3e", null ],
    [ "userT", "lab0x02_8py.html#a55800046bdfee569599491e5ae8b7b0f", null ]
];